<? require "php/bd.php"?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/main.css">
	<title>Тестовое задание Альтерра</title>
</head>
<body class="wrapper">
	<div class="content">
		<section class="content__part content__form">	
			<header class="content__header">
				<h1 class="content__title">
					Добавить контакт
				</h1>
			</header>
			<form method="post" action="php/create.php" class="form create-form">
				<label class="form__label">
					<input name="name" type="text" class="form__input create-name" placeholder="Имя" required>
					<!-- <input name="name" type="text" class="form__input " placeholder="Имя" > -->
				</label>
				<label class="form__label">
					<input name="phone" type="tel" class="form__input create-phone" placeholder="Телефон: 89007006050" required pattern="[0-9]{11}">
					<!-- <input name="phone" type="tel" class="form__input " placeholder="Телефон" > -->
				</label>
				<button class="form__submit" type="submit">
					Добавить
				</button>
			</form>

		</section>
		<section class="content__part content__list">	
			<header class="content__header">
				<h2 class="content__title">
					Список контактов
				</h2>
			</header>
			<div class="contacts">
				<!-- Для каждого элемента подстваляется id -->
				<?
					$list_contacts = $mysqli->query("SELECT * FROM contacts");
					$post = array();
					while($row = mysqli_fetch_assoc($list_contacts)){
						$posts[] = $row;
					}

					if (isset($posts)){
						foreach ($posts as $post){
							?>
							<div data-key="<? echo($post['id']);?>" class="contacts__one one-contact">
								<p class="one-contact__name">
									<span>
										<? echo($post['name']);?>
									</span>
									<button class="one-contact__del">×</button>
								</p>
								<a href="tel:<? echo($post['name']);?>" class="one-contact__phone"><? echo($post['phone']);?></a>
							</div>			
							<?
						}
					} else {
						echo "<div class=\"content__header\"><h2 class=\"content__title\">Контактов пока нет</h2></div>";
					}
				?>
				
			</div>
		</section>
		
	</div>
	<form method="POST" action="/build/php/remove.php" class="remove-form" hidden>
		<input class="target" type="number" name="target">
	</form>
	<script src="js/main.js"></script>
</body>
</html>